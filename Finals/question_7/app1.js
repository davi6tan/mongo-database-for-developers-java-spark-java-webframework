use m101Java

/* Sets the index for faster look-up at albums */
db.albums.ensureIndex({'images': 1});

db.images.find().forEach(function (myDoc) {
    // print("user: " + myDoc.tags); use limit(10) for testing
    if(db.albums.find({images: myDoc._id}).count() == 0){
        db.images.remove({_id: myDoc._id})
    }
});

print("#completed")