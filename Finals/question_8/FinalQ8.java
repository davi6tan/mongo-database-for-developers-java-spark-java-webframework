package course;

/**
 * Created by davidtan on 5/1/15.
 */

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class FinalQ8 {
    public static void main(String[] args) {

        MongoClient c = new MongoClient();
        MongoDatabase db = c.getDatabase("test");
        MongoCollection<Document> animals = db.getCollection("animals");

        Document animal = new Document("animal", "monkey");
        animals.insertOne(animal);
        animal.remove("animal");
        animal.append("animal", "cat");
        animals.insertOne(animal);
        animal.remove("animal");
        animal.append("animal", "lion");
        animals.insertOne(animal);
    }
}

/*
Last login: Fri May  1 20:27:35 on ttys005
Davids-MacBook-Pro:coursera davidtan$ mongo
MongoDB shell version: 2.6.6
connecting to: test
> use course2
switched to db course2
> db.insertTest3.find()
{ "_id" : ObjectId("55442905d4c6efa244e9596a"), "y" : 1 }
> use test
switched to db test
> db.animals.find()
{ "_id" : ObjectId("55442bfd9793541d3497f09f"), "animal" : "monkey" }
>
Exception in thread "main" com.mongodb.MongoWriteException: insertDocument :: caused by ::
11000 E11000 duplicate key error index: test.animals.$_id_  dup key: { : ObjectId('55442bfd9793541d3497f09f') }
	at com.mongodb.MongoCollectionImpl.executeSingleWriteRequest(MongoCollectionImpl.java:479)
	at com.mongodb.MongoCollectionImpl.insertOne(MongoCollectionImpl.java:276)
 */