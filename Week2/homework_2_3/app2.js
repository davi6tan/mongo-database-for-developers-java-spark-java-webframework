use studentsJ

var student_id = 1000;
db.grades
    .find({'type': 'homework'})
    .sort({'student_id': 1, 'score': 1,})
    .forEach(function (myDoc) {
        if (student_id != myDoc.student_id) {
            student_id = myDoc.student_id;
            print("id : " + myDoc.student_id + ", type : " + myDoc.type + ", score : " + myDoc.score);
            db.grades.remove({_id: myDoc._id})
        }
    });

/*
 Ascending 1 descending -1 (hi-lo)
 {'student_id':1, 'score':1, }

 To run
 goto this directory, make sure mongo is running -> mongod

 cat app2.js | mongo
 */