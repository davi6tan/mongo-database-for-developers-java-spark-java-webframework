/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;

import java.net.UnknownHostException;
import java.util.Random;

public class FindCriteriaTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection collection = db.getCollection("findCriteriaTest");
        collection.drop();

        // insert 10 documents with two random integers
        for (int i = 0; i < 10; i++) {
            collection.insert(
                    new BasicDBObject("x", new Random().nextInt(2))
                            .append("y", new Random().nextInt(100)));
        }

        QueryBuilder builder = QueryBuilder.start("x").is(0)
                .and("y").greaterThan(10).lessThan(70);
        DBObject query = new BasicDBObject("x", 0)
                .append("y", new BasicDBObject("$gt", 10).append("$lt", 90));

        System.out.println("\nCount:");
        long count = collection.count(builder.get());
        System.out.println(count);

        System.out.println("\nFind all: ");
        DBCursor cursor = collection.find(builder.get());
        try {
            while (cursor.hasNext()) {
                DBObject cur = cursor.next();
                System.out.println(cur);
            }
        } finally {
            cursor.close();
        }
    }
}
/*
Count:
3

Find all:
{ "_id" : { "$oid" : "5512f20fd4c6db2a1f3d4a7e"} , "x" : 0 , "y" : 16}
{ "_id" : { "$oid" : "5512f20fd4c6db2a1f3d4a7f"} , "x" : 0 , "y" : 24}
{ "_id" : { "$oid" : "5512f20fd4c6db2a1f3d4a83"} , "x" : 0 , "y" : 40}

> db.findCriteriaTest.find()
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7a"), "x" : 0, "y" : 82 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7b"), "x" : 1, "y" : 23 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7c"), "x" : 1, "y" : 18 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7d"), "x" : 0, "y" : 89 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7e"), "x" : 0, "y" : 16 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a7f"), "x" : 0, "y" : 24 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a80"), "x" : 0, "y" : 73 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a81"), "x" : 0, "y" : 70 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a82"), "x" : 0, "y" : 97 }
{ "_id" : ObjectId("5512f20fd4c6db2a1f3d4a83"), "x" : 0, "y" : 40 }

 */