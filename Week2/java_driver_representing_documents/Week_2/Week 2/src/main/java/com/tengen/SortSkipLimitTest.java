/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.Random;

public class SortSkipLimitTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection lines = db.getCollection("SortSkipLimitTest");
        lines.drop();
        Random rand = new Random();

        // insert 10 lines with random start and end points
        for (int i = 0; i < 10; i++) {
            lines.insert(
                    new BasicDBObject("_id", i)
                            .append("start",
                                    new BasicDBObject("x", rand.nextInt(2))
                                              .append("y", rand.nextInt(90) + 10)
                            )
                            .append("end",
                                    new BasicDBObject("x", rand.nextInt(2))
                                              .append("y", rand.nextInt(90) + 10)
                            )
            );
        }

        DBCursor cursor = lines.find()
                .sort(new BasicDBObject("start.x", 1).append("start.y", -1))
                .skip(2).limit(10);

        try {
            while (cursor.hasNext()) {
                DBObject cur = cursor.next();
                System.out.println(cur);
            }
        } finally {
            cursor.close();
        }
    }
}
/*
{ "_id" : 0 , "start" : { "x" : 0 , "y" : 42} , "end" : { "x" : 1 , "y" : 62}}
{ "_id" : 3 , "start" : { "x" : 0 , "y" : 32} , "end" : { "x" : 0 , "y" : 14}}
{ "_id" : 5 , "start" : { "x" : 0 , "y" : 22} , "end" : { "x" : 1 , "y" : 14}}
{ "_id" : 2 , "start" : { "x" : 1 , "y" : 98} , "end" : { "x" : 0 , "y" : 44}}
{ "_id" : 9 , "start" : { "x" : 1 , "y" : 91} , "end" : { "x" : 1 , "y" : 50}}
{ "_id" : 4 , "start" : { "x" : 1 , "y" : 56} , "end" : { "x" : 1 , "y" : 94}}
{ "_id" : 6 , "start" : { "x" : 1 , "y" : 53} , "end" : { "x" : 1 , "y" : 11}}
{ "_id" : 8 , "start" : { "x" : 1 , "y" : 10} , "end" : { "x" : 1 , "y" : 43}}
Notes
> db.SortSkipLimitTest.find()
{ "_id" : 0, "start" : { "x" : 0, "y" : 42 }, "end" : { "x" : 1, "y" : 62 } }
{ "_id" : 1, "start" : { "x" : 0, "y" : 59 }, "end" : { "x" : 0, "y" : 89 } }
{ "_id" : 2, "start" : { "x" : 1, "y" : 98 }, "end" : { "x" : 0, "y" : 44 } }
{ "_id" : 3, "start" : { "x" : 0, "y" : 32 }, "end" : { "x" : 0, "y" : 14 } }
{ "_id" : 4, "start" : { "x" : 1, "y" : 56 }, "end" : { "x" : 1, "y" : 94 } }
{ "_id" : 5, "start" : { "x" : 0, "y" : 22 }, "end" : { "x" : 1, "y" : 14 } }
{ "_id" : 6, "start" : { "x" : 1, "y" : 53 }, "end" : { "x" : 1, "y" : 11 } }
{ "_id" : 7, "start" : { "x" : 0, "y" : 43 }, "end" : { "x" : 0, "y" : 14 } }
{ "_id" : 8, "start" : { "x" : 1, "y" : 10 }, "end" : { "x" : 1, "y" : 43 } }
{ "_id" : 9, "start" : { "x" : 1, "y" : 91 }, "end" : { "x" : 1, "y" : 50 } }


 */