/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;

import java.net.UnknownHostException;
import java.util.Random;

public class DotNotationTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection lines = db.getCollection("DotNotationTest");
        lines.drop();
        Random rand = new Random();

        // insert 10 lines with random start and end points
        for (int i = 0; i < 10; i++) {
            lines.insert(
                    new BasicDBObject("_id", i)
                            .append("start",
                                    new BasicDBObject("x", rand.nextInt(90) + 10)
                                            .append("y", rand.nextInt(90) + 10)
                            )
                            .append("end",
                                    new BasicDBObject("x", rand.nextInt(90) + 10)
                                            .append("y", rand.nextInt(90) + 10)
                            )
            );
        }

        QueryBuilder builder = QueryBuilder.start("start.x").greaterThan(50);

        DBCursor cursor = lines.find(builder.get(),
                new BasicDBObject("start.y", true).append("_id", false));

        try {
            while (cursor.hasNext()) {
                DBObject cur = cursor.next();
                System.out.println(cur);
            }
        } finally {
            cursor.close();
        }
    }
}
/*
{ "start" : { "y" : 66}}
{ "start" : { "y" : 93}}
{ "start" : { "y" : 26}}

Notes
Davids-MacBook-Pro:mongo101-java-python davidtan$ mongo
MongoDB shell version: 2.6.6
connecting to: test
> use course
switched to db course
> db.DotNotationTest.find()
{ "_id" : 0, "start" : { "x" : 47, "y" : 61 }, "end" : { "x" : 86, "y" : 41 } }
{ "_id" : 1, "start" : { "x" : 40, "y" : 80 }, "end" : { "x" : 79, "y" : 15 } }
{ "_id" : 2, "start" : { "x" : 25, "y" : 96 }, "end" : { "x" : 13, "y" : 21 } }
{ "_id" : 3, "start" : { "x" : 49, "y" : 97 }, "end" : { "x" : 18, "y" : 32 } }
{ "_id" : 4, "start" : { "x" : 32, "y" : 94 }, "end" : { "x" : 36, "y" : 13 } }
{ "_id" : 5, "start" : { "x" : 93, "y" : 66 }, "end" : { "x" : 21, "y" : 61 } }
{ "_id" : 6, "start" : { "x" : 91, "y" : 93 }, "end" : { "x" : 39, "y" : 81 } }
{ "_id" : 7, "start" : { "x" : 98, "y" : 26 }, "end" : { "x" : 12, "y" : 94 } }
{ "_id" : 8, "start" : { "x" : 50, "y" : 42 }, "end" : { "x" : 75, "y" : 30 } }
{ "_id" : 9, "start" : { "x" : 37, "y" : 47 }, "end" : { "x" : 33, "y" : 22 } }
>


 */