/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;

import java.net.UnknownHostException;
import java.util.Random;

public class FieldSelectionTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection collection = db.getCollection("fieldSelectionTest");
        collection.drop();
        Random rand = new Random();

        // insert 10 documents with two random integers
        for (int i = 0; i < 10; i++) {
            collection.insert(
                    new BasicDBObject("x", rand.nextInt(2))
                            .append("y", rand.nextInt(100))
                            .append("z", rand.nextInt(1000)));
        }

        DBObject query = QueryBuilder.start("x").is(0)
                .and("y").greaterThan(10).lessThan(70).get();

        DBCursor cursor = collection.find(query,
                new BasicDBObject("y", true).append("_id", false));
        try {
            while (cursor.hasNext()) {
                DBObject cur = cursor.next();
                System.out.println(cur);
            }
        } finally {
            cursor.close();
        }
    }
}
/*
{ "y" : 61}
{ "y" : 69}

Notes

> db.fieldSelectionTest.find()
{ "_id" : ObjectId("5512f1add4c6dabc596df77a"), "x" : 1, "y" : 33, "z" : 734 }
{ "_id" : ObjectId("5512f1add4c6dabc596df77b"), "x" : 1, "y" : 64, "z" : 158 }
{ "_id" : ObjectId("5512f1add4c6dabc596df77c"), "x" : 0, "y" : 61, "z" : 26 }
{ "_id" : ObjectId("5512f1add4c6dabc596df77d"), "x" : 0, "y" : 69, "z" : 717 }
{ "_id" : ObjectId("5512f1add4c6dabc596df77e"), "x" : 1, "y" : 60, "z" : 11 }
{ "_id" : ObjectId("5512f1add4c6dabc596df77f"), "x" : 0, "y" : 99, "z" : 384 }
{ "_id" : ObjectId("5512f1add4c6dabc596df780"), "x" : 1, "y" : 60, "z" : 534 }
{ "_id" : ObjectId("5512f1add4c6dabc596df781"), "x" : 0, "y" : 87, "z" : 591 }
{ "_id" : ObjectId("5512f1add4c6dabc596df782"), "x" : 1, "y" : 30, "z" : 210 }
{ "_id" : ObjectId("5512f1add4c6dabc596df783"), "x" : 1, "y" : 37, "z" : 911 }
 */