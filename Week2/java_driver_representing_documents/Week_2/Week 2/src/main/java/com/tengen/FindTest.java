/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.Random;

public class FindTest {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB db = client.getDB("course");
        DBCollection collection = db.getCollection("findTest");
        collection.drop();

        // insert 10 documents with a random integer as the value of field "x"
        for (int i = 0; i < 10; i++) {
            collection.insert(new BasicDBObject("x", new Random().nextInt(100)));
        }

        System.out.println("Find one:");
        DBObject one = collection.findOne();
        System.out.println(one);

        System.out.println("\nFind all: ");
        DBCursor cursor = collection.find();
        try {
            while (cursor.hasNext()) {
                DBObject cur = cursor.next();
                System.out.println(cur);
            }
        } finally {
            cursor.close();
        }

        System.out.println("\nCount:");
        long count = collection.count();
        System.out.println(count);
    }
}
/*
Find one:
{ "_id" : { "$oid" : "5512f23dd4c62d864bba457d"} , "x" : 72}

Find all:
{ "_id" : { "$oid" : "5512f23dd4c62d864bba457d"} , "x" : 72}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba457e"} , "x" : 65}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba457f"} , "x" : 24}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4580"} , "x" : 57}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4581"} , "x" : 41}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4582"} , "x" : 61}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4583"} , "x" : 59}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4584"} , "x" : 77}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4585"} , "x" : 58}
{ "_id" : { "$oid" : "5512f23dd4c62d864bba4586"} , "x" : 36}

Count:
10

Notes
db.findTest.find()
> db.findTest.find()
{ "_id" : ObjectId("5512f23dd4c62d864bba457d"), "x" : 72 }
{ "_id" : ObjectId("5512f23dd4c62d864bba457e"), "x" : 65 }
{ "_id" : ObjectId("5512f23dd4c62d864bba457f"), "x" : 24 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4580"), "x" : 57 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4581"), "x" : 41 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4582"), "x" : 61 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4583"), "x" : 59 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4584"), "x" : 77 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4585"), "x" : 58 }
{ "_id" : ObjectId("5512f23dd4c62d864bba4586"), "x" : 36 }
 */