use blog;
db.posts.aggregate(
    { $unwind: "$comments" },
    { $group: { _id: "$comments", "count": { $sum: 1 }}},
    { $sort: { "count": 1 }}, { $limit: 3},
    {$project : {_id:0, 'author':'$_id', 'count':1}}
);


