use test
db.zips.aggregate([
    {$project:
    {
        first_char: {$substr : ["$city",0,1]},
    }
    }
])

//Davids-MacBook-Pro:homework_5_4 davidtan$ ls
//app.js                          homework_5_4_Instructions       zips.json
//app0.js                         notes
//Davids-MacBook-Pro:homework_5_4 davidtan$ cat app0.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db test
//{ "_id" : "35004", "first_char" : "A" }
//{ "_id" : "35005", "first_char" : "A" }
//{ "_id" : "35006", "first_char" : "A" }
//{ "_id" : "35007", "first_char" : "K" }
//{ "_id" : "35010", "first_char" : "N" }
//{ "_id" : "35014", "first_char" : "A" }
//{ "_id" : "35016", "first_char" : "A" }
//{ "_id" : "35019", "first_char" : "B" }
//{ "_id" : "35020", "first_char" : "B" }
//{ "_id" : "35023", "first_char" : "H" }
//{ "_id" : "35031", "first_char" : "B" }
//{ "_id" : "35033", "first_char" : "B" }
//{ "_id" : "35034", "first_char" : "B" }
//{ "_id" : "35035", "first_char" : "B" }
//{ "_id" : "35040", "first_char" : "C" }
//{ "_id" : "35042", "first_char" : "C" }
//{ "_id" : "35043", "first_char" : "C" }
//{ "_id" : "35044", "first_char" : "C" }
//{ "_id" : "35045", "first_char" : "C" }
//{ "_id" : "35049", "first_char" : "C" }
//Type "it" for more
//    bye
//Davids-MacBook-Pro:homework_5_4 davidtan$