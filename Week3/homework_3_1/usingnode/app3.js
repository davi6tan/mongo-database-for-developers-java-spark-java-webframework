var MongoClient = require('mongodb').MongoClient,
    _ = require('lodash');// npm install lodash --save

MongoClient.connect('mongodb://localhost:27017/schoolJ', function (err, db) {
    if (err) throw err;

    var cursor = db.collection('students').find({});

    cursor.each(function (err, doc) {
        /* exceptions and errors*/
        if (err) throw err;
        if (!doc)return db.close();
        var out =[]//object out
        doc.scores.map(function (x, i) {if (x.type === 'homework')out.push({"scorez":x.score,"pos":i});});
        //https://lodash.com/docs#min
        /* find minimum position for splicing*/
        var min_position= _.min(out, function (p) {return p.scorez;}).pos;
        if(min_position){
            //console.log(min_position)
            doc.scores.splice(min_position, 1);
            db.collection('students').save(doc, function (err, saved) {if (err) throw err;});
        }
    }); //each document

});

/*
 mongoimport -d schoolJ -c students --drop students.json

 to run node app3.js

ignore the throw error

Before
 > db.students.aggregate( { '$unwind' : '$scores' } , { '$group' : { '_id' : '$_id' , 'average' : { $avg : '$scores.score' } } } , { '$sort' : { 'average' : -1 } } , { '$limit' : 1 } )
 { "_id" : 64, "average" : 90.21930337096714 }
 After

 > db.students.aggregate( { '$unwind' : '$scores' } , { '$group' : { '_id' : '$_id' , 'average' : { $avg : '$scores.score' } } } , { '$sort' : { 'average' : -1 } } , { '$limit' : 1 } )
 { "_id" : 13, "average" : 91.98315917172745 }
 >

 */