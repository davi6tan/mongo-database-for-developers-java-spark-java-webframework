use schoolJ

db.students.find({}).forEach(function (doc) {
    var min = 101;
    var min_i = null;

    /* find the minimum score of homework -> min_i*/
    doc.scores.map(function (homework, i) {
        if (homework.type === "homework") {
            if (min > homework.score) {
                min = homework.score;
                min_i = i
            }
        }
    })

    /* if not null*/
    if (min_i) {
       // print("min " + min + "temp " + min_i);
        doc.scores.splice(min_i, 1);
        db.students.save(doc, function (err, saved) {
            if (err) throw err;
        });
    }

});


/*
 mongoimport -d schoolJ -c students --drop students.json

 to run --> cat app.js | mongo

 Before
 > db.students.aggregate( { '$unwind' : '$scores' } , { '$group' : { '_id' : '$_id' , 'average' : { $avg : '$scores.score' } } } , { '$sort' : { 'average' : -1 } } , { '$limit' : 1 } )
 { "_id" : 64, "average" : 90.21930337096714 }

 cat app.js | mongo
 > db.students.aggregate( { '$unwind' : '$scores' } , { '$group' : { '_id' : '$_id' , 'average' : { $avg : '$scores.score' } } } , { '$sort' : { 'average' : -1 } } , { '$limit' : 1 } )
 { "_id" : 13, "average" : 91.98315917172745 }

 */