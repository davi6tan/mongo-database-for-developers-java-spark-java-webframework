/**
 * Created by davidtan on 4/2/15.
 */
var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/schoolJ', function (err, db) {
    if (err) throw err;

    var cursor = db.collection('students').find({});

    cursor.each(function (err, doc) {
        if (err) throw err;

        if (doc == null) {
            return db.close();
        }
        var min = 101.0000;
        var ctr = 0;//reset
        var minL = 0; //minimum location at scores

        doc.scores.filter(function (x, i) {
            if (x.type === 'homework') {
                if (ctr === 0) {
                    min = x.score;
                }
                if (x.score < min) {
                    min = x.score;
                    minL = i;
                }
                ctr++;
            }
        });

        //Update new document
        if (ctr > 1) { // splice only when > 1
            doc.scores.splice(minL, 1);
            db.collection('students').save(doc, function (err, saved) {
                if (err) throw err;
                console.log("Number of document saved successfully : " + saved);

            });
        }


    }); //each document

});

/*
Working April 02, 2015

 > db.students.aggregate( { '$unwind' : '$scores' } , { '$group' : { '_id' : '$_id' , 'average' : { $avg : '$scores.score' } } } , { '$sort' : { 'average' : -1 } } , { '$limit' : 1 } )
 { "_id" : 13, "average" : 91.98315917172745 }
 >

 */